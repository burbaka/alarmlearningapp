package com.tts.reader.alarm.learning;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;


public class AboutActivity extends Activity {

    private static final String	TAG	= "AboutActivity";

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate(Bundle savedInstanceState)");
        setContentView(R.layout.about_activity);


    }
}
