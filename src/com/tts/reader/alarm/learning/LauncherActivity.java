package com.tts.reader.alarm.learning;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class LauncherActivity extends Activity implements View.OnClickListener {
	private static final String	TAG	= "LauncherActivity";

	private Button				vBtnAbout;
	private Button				vBtnMethod;
	private Button				vBtnStart;
	private Button				vBtnContact;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate(Bundle savedInstanceState)");
		setContentView(R.layout.launcher_activity);

		vBtnAbout = (Button) findViewById(R.id.btn_about);
		vBtnAbout.setOnClickListener(this);

		vBtnMethod = (Button) findViewById(R.id.btn_method);
		vBtnMethod.setOnClickListener(this);

		vBtnContact = (Button) findViewById(R.id.btn_contact);
		vBtnContact.setOnClickListener(this);

		vBtnStart = (Button) findViewById(R.id.btn_start);
		vBtnStart.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		Log.v(TAG, "onClick(View v)");

		Intent intent;
		switch (v.getId()) {
		case R.id.btn_about:
			intent = new Intent(this, AboutActivity.class);
			startActivity(intent);
			break;
		case R.id.btn_method:
			intent = new Intent(this, MethodActivity.class);
			startActivity(intent);
			break;
		case R.id.btn_contact:
			intent = new Intent(this, ContactActivity.class);
			startActivity(intent);
			break;
		case R.id.btn_start:
			intent = new Intent(this, StartActivity.class);
			startActivity(intent);
			break;
		}

	}
}
