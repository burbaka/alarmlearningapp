package com.tts.reader.alarm.learning;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

public class StartActivity extends Activity {


    private static final String	TAG	= "StartActivity";


    private MediaPlayer mPlayer;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate(Bundle savedInstanceState)");
        setContentView(R.layout.start_activity);

        mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.chapter_1);
        mPlayer.setLooping(true);
        mPlayer.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");

        mPlayer.stop();
        mPlayer.release();
    }
}
